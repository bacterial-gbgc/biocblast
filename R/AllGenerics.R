
setGeneric("blast",
           function(query, subject, ..., param = blastParam())
             standardGeneric("blast"),
           signature = c("query", "subject"))

setGeneric("blastDb",
           function(subject, param = blastDbParam(), ...)
             standardGeneric("blastDb"))

setGeneric("diamondDb",
           function(subject, param = diamondDbParam(), ...)
             standardGeneric("diamondDb"))

setGeneric("searchOptions",
           function(param, ...) standardGeneric("searchOptions"))

setGeneric("searchOptions<-",
           function(param, ..., value) standardGeneric("searchOptions<-"))

setGeneric("formatOptions",
           function(param, ...) standardGeneric("formatOptions"))

setGeneric("formatOptions<-",
           function(param, ..., value) standardGeneric("formatOptions<-"))

setGeneric("restrictOptions",
           function(param, ...) standardGeneric("restrictOptions"))

setGeneric("restrictOptions<-",
           function(param, ..., value) standardGeneric("restrictOptions<-"))
